import { Document } from 'mongoose';

export interface IEncuesta extends Document {
	pregunta1: String;
	pregunta2: Number;
	pregunta3: String;
	pregunta4: Number;
	pregunta5A: Number;
	pregunta5B: Number;
	pregunta5C: Number;
	pregunta5D: Number;
	pregunta5E: Number;
	pregunta5F: Number;
	pregunta5G: Number;
	pregunta5H: Number;
	pregunta5I: Number;
	pregunta6: Number;
	pregunta7: Number;
	pregunta8: Number;
	pregunta9: Number;
	idPaciente: String;
	resultado: Number;
}
