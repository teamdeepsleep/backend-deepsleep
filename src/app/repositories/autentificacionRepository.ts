import { compareSync } from 'bcryptjs';
import { crearToken } from '../helpers/jwt';
import Paciente from '../models/schemas/pacienteModel';
import Especialista from '../models/schemas/especialistaModel';

export const loginPaciente = async (correo: string, password: string) => {
	try {
		const pacienteBD = await Paciente.findOne({ correo }).select(
			'correo dni password',
		);

		if (!pacienteBD) {
			return {
				operacion: false,
				encontrado: false,
				mensaje: 'El usuario no se encuentra registrado',
			};
		}
		// console.log('usuario encontrado', pacienteBD);
		// console.log('usuario ingresado', password, pacienteBD.password);

		const validarPassword = compareSync(password, pacienteBD.password);
		console.log('compare is', validarPassword);

		if (!validarPassword) {
			return {
				operacion: false,
				encontrado: true,
				mensaje: 'La contraseña es incorrecta',
			};
		}

		//guardar token
		const token = await crearToken(pacienteBD._id, 'paciente');
		return {
			operacion: true,
			encontrado: true,
			mensaje: 'Operación de Logueo exitoso',
			token: token,
			id: pacienteBD._id,
			tipo: 'paciente',
		};
	} catch (error) {
		console.log('err usuario', error);
		return {};
	}
};

export const loginEspecialista = async (correo: string, password: string) => {
	try {
		const especialistaBD = await Especialista.findOne({ correo }).select(
			'correo dni password',
		);

		if (!especialistaBD) {
			return {
				operacion: false,
				encontrado: false,
				mensaje: 'El especialista no se encuentra registrado',
			};
		}
		const validarPassword = compareSync(password, especialistaBD.password);
		console.log('compare is', validarPassword);

		if (!validarPassword) {
			return {
				operacion: false,
				encontrado: true,
				mensaje: 'La contraseña es incorrecta',
			};
		}

		//guardar token
		const token = await crearToken(especialistaBD._id, 'especialista');
		return {
			operacion: true,
			encontrado: true,
			mensaje: 'Operación de Logueo exitoso',
			token: token,
			id: especialistaBD._id,
			tipo: 'especialista',
		};
	} catch (error) {
		console.log('err especialista', error);
		return {};
	}
};

export const reNewToken = async (_id: string) => {
	try {
	} catch (error) {}
};
