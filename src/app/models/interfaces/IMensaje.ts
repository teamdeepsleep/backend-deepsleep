import { Document, Schema } from 'mongoose';

export interface IMensaje extends Document {
	mensaje: string;
	idPaciente: string;
	idEspecialista: string;
}
