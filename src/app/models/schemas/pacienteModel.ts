import { Schema, model } from 'mongoose';
import { IPaciente } from '../interfaces/IPaciente';

var pacienteSchema = new Schema(
	{
		nombre: {
			type: String,
			required: true,
		},
		apellido: {
			type: String,
			required: true,
		},
		dni: {
			type: String,
			required: true,
			unique: true,
		},
		correo: {
			type: String,
			required: true,
			unique: true,
		},
		password: {
			type: String,
			required: true,
			select: false,
		},
		imagen: {
			type: String,
			required: false,
		},
		telefono: {
			type: String,
			required: false,
		},
		idEspecialista: {
			type: String,
			required: false,
			default: '',
		},
		habilitado: {
			type: Boolean,
			default: true,
		},
		psqi: {
			type: Number,
			required: false,
			default: 0,
		},
		recomendaciones: [
			{
				type: Schema.Types.ObjectId,
				ref: 'recomendaciones',
			},
		],
	},
	{
		collection: 'usuarios',
	},
);

pacienteSchema.methods.encriptarPassword = (password: string) => {
	console.log(password);
};

var Paciente = model<IPaciente>('pacientes', pacienteSchema);
export default Paciente;
