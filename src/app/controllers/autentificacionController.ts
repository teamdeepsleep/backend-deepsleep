import { Request, Response } from 'express';
import {
	loginPaciente,
	loginEspecialista,
	reNewToken,
} from '../repositories/autentificacionRepository';

export class AutentificacionController {
	constructor() {}
	login = async (req: Request, res: Response) => {
		const { correo, password } = req.body;
		var respuesta = await loginPaciente(correo, password);
		var respuesta2 = await loginEspecialista(correo, password);
		console.log('res 1', respuesta);
		console.log('res 2', respuesta2);

		if (
			respuesta.operacion == false &&
			respuesta.encontrado == false &&
			respuesta2.operacion == false &&
			respuesta2.encontrado == false
		) {
			return res.json({
				operacion: false,
				mensaje: 'La cuenta no se encuentra registrado',
			});
		}

		// if (respuesta.operacion == false && respuesta.encontrado == false) {
		// 	return res.json(respuesta);
		// }

		// if (respuesta2.operacion == false && respuesta2.encontrado == false) {
		// 	return res.json(respuesta2);
		// }

		if (
			// respuesta.operacion == false &&
			respuesta.encontrado == true
		) {
			console.log('encontre usuario');
			return res.json(respuesta);
		} else {
			console.log('encontre especialista');
			return res.json(respuesta2);
		}

	};
	loginPaciente = async (req: Request, res: Response) => {
		const { correo, password } = req.body;
		var respuesta = await loginPaciente(correo, password);
		return res.json(respuesta);
	};

	loginEspecialista = async (req: Request, res: Response) => {
		const { correo, password } = req.body;
		var respuesta = await loginEspecialista(correo, password);
		return res.json(respuesta);
	};

	reNewToken = async (req: Request, res: Response) => {
		const id = req.params._id;
		var respuesta = await reNewToken(id);
		return res.json(respuesta);
	};
}
