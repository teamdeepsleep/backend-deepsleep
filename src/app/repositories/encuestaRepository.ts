import { ObjectId } from 'bson';
import Encuesta from '../models/schemas/encuestaModel';
import Paciente from '../models/schemas/pacienteModel';

export const registrarEncuesta = async (
	idPaciente: ObjectId,
	encuesta: any,
	resultado: number,
) => {
	try {
		const encuestaNEW = new Encuesta(encuesta);
		//Guardar encuesta
		var data = await encuestaNEW.save();

		//Actualizar paciente
		await Paciente.findByIdAndUpdate(
			{ _id: idPaciente },
			{
				psqi: resultado,
			},
			{ new: true },
		);

		return data;
	} catch (error) {
		return null;
	}
};
