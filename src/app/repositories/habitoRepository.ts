import { ObjectId } from 'bson';
import { IHabito } from '../models/interfaces/IHabito';
import Habito from '../models/schemas/habitoModel';
import HabitoPaciente from '../models/schemas/habitoPacienteModel';
import { IRespuesta } from '../helpers/IRespuesta';
import { IHabitoPaciente } from '../models/interfaces/IHabito_Paciente';

export const listarHabitos = async () => {
	try {
		var data = await Habito.find();
		return {
			operacion: true,
			data: data,
			mensaje: 'lista encontrada',
		} as IRespuesta<IHabito[]>;
	} catch (error) {
		return {
			operacion: false,
			data: [],
			mensaje: 'lista no encontrada',
		} as IRespuesta<IHabito[]>;
	}
};

export const listarPorPaciente = async (idPaciente: ObjectId) => {
	try {
		var data: any[] = [];
		var datos = await HabitoPaciente.find({ idPaciente: idPaciente });
		var habitos = await Habito.find();

		// no tiene ningun habito
		if (datos.length == 0) {
			habitos.map((habito: IHabito) => {
				data.push({
					_id: habito.id,
					nombre: habito.nombre,
					imagen: habito.imagen,
					estado: false,
				});
			});

			return {
				operacion: true,
				data: data,
				mensaje: 'lista encontrada',
			};
		}

		habitos.map(async (hab: IHabito) => {
			var encontrado = await datos.find((dat: any) => {
				return dat.idHabito._id.toString() === hab._id._id.toString();
			});

			if (encontrado === undefined) {
				data.push({
					_id: hab._id,
					nombre: hab.nombre,
					imagen: hab.imagen,
					estado: false,
				});
			} else {
				data.push({
					_id: hab._id,
					nombre: hab.nombre,
					imagen: hab.imagen,
					estado: true,
				});
			}
		});

		return {
			operacion: true,
			data: data,
			mensaje: 'lista encontrada',
		};
	} catch (error) {
		return {
			operacion: false,
			data: [],
			mensaje: 'lista no encontrada',
		};
	}
};

export const listarHabitoPorId = async (id: ObjectId) => {
	try {
		var data = await Habito.findById(id);

		return {
			operacion: true,
			data: data,
			mensaje: 'habito encontrado',
		} as IRespuesta<IHabito>;
	} catch (error) {
		return {
			operacion: false,
			data: null,
			mensaje: 'habito no encontrado',
		} as IRespuesta<IHabito>;
	}
};

export const crearHabito = async (habito: any) => {
	try {
		const habitoNEW = new Habito(habito);
		var data = await habitoNEW.save();

		return {
			operacion: true,
			data: data,
			mensaje: 'habito creado',
		} as IRespuesta<IHabito>;
	} catch (error) {
		return {
			operacion: false,
			data: data,
			mensaje: 'habito no creado',
		} as IRespuesta<IHabito>;
	}
};

export const crearHabitoPorPaciente = async (
	habitos: any,
	idPaciente: ObjectId,
) => {
	try {
		var misHabitos = await HabitoPaciente.find({
			idPaciente: idPaciente,
		}).exec();

		habitos.map(async (h: any) => {
			var habit = JSON.parse(h);
			var encontre = await misHabitos.find(
				(x) => x.idHabito._id.toString() == habit._id,
			);

			if (habit.estado == true) {
				if (encontre) {
					console.log('no insertar, ya existe', habit._id);
				} else {
					console.log('insertar', habit._id);
					const habitoPacienteNEW = new HabitoPaciente({
						idPaciente: idPaciente,
						idHabito: habit._id,
					});
					var d = await habitoPacienteNEW.save();
					console.log('insert result', d);
				}
			} else {
				if (encontre) {
					console.log('eliminar', habit._id);
					var d = await HabitoPaciente.findOneAndDelete({
						idHabito: habit._id,
						idPaciente: idPaciente,
					});
					console.log('eliminado result', d);
				} else {
					console.log('no guardar', habit._id);
				}
			}
		});

		return {
			operacion: true,
			mensaje: 'habitos guardados',
		};
	} catch (error) {
		return {
			operacion: false,
			data: null,
			mensaje: 'habitos no gaurdados',
		};
	}
};

export const actualizarHabito = async (id: ObjectId, habito: any) => {
	try {
		var data = await Habito.findByIdAndUpdate({ _id: id }, habito, {
			new: true,
		});

		return {
			operacion: true,
			data: data,
			mensaje: 'actualizado',
		} as IRespuesta<IHabito>;
	} catch (error) {
		return {
			operacion: false,
			data: null,
			mensaje: 'no actualizado',
		} as IRespuesta<IHabito>;
	}
};

export const eliminarHabito = async (id: ObjectId) => {
	try {
		var data = await Habito.findByIdAndDelete(id);

		return {
			operacion: true,
			data: data,
			mensaje: 'eliminado',
		} as IRespuesta<IHabito>;
	} catch (error) {
		return {
			operacion: false,
			data: null,
			mensaje: 'no eliminado',
		} as IRespuesta<IHabito>;
	}
};
