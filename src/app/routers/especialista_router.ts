import { Router } from 'express';
import { EspecialistaController } from '../controllers/especialistaController';

export const router = Router();
const especialistaController: EspecialistaController =
	new EspecialistaController();

router.get('/', especialistaController.listarTodos);

router.post('/', especialistaController.crear);

router.get('/activos', especialistaController.listarActivos);

router.put('/estado', especialistaController.cambiarEstado);


router.get(
	'/mispacientes/:idespecialista',
	especialistaController.mispacientes,
);

router.get(
	'/misposiblespacientes/:idespecialista',
	especialistaController.misposiblespacientes,
);
router.get(
	'/detalle/:idespecialista',
	especialistaController.detalleEspecialista,
);
router.put(
	'/:idespecialista/miposiblepaciente/:idpaciente/aceptar',
	especialistaController.miPosiblePacienteAceptar,
);

router.put(
	'/:idespecialista/agregarEspecialidad',
	especialistaController.agregarEspecialidad,
);

router.put(
	'/:idespecialista/eliminarEspecialidad',
	especialistaController.eliminarEspecialidad,
);

router.put(
	'/:idespecialista/actualizarEspecialista',
	especialistaController.actualizarEspecialistaDetalle,
);


// router.put('/estado', especialistaController.cambiarEstado);

router.put('/:id', especialistaController.actualizar);
router.get('/:id', especialistaController.listarPorId);
