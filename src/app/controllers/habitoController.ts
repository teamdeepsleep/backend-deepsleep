import { Request, Response } from 'express';
import { IHabito } from '../models/interfaces/IHabito';
import {
	listarHabitos,
	listarHabitoPorId,
	crearHabito,
	actualizarHabito,
	eliminarHabito,
	listarPorPaciente,
	crearHabitoPorPaciente,
} from '../repositories/habitoRepository';
export class HabitoController {
	constructor() {}

	listarTodos = async (req: Request, res: Response) => {
		var respuesta = await listarHabitos();
		return res.json(respuesta);
	};

	listarPorPaciente = async (req: Request, res: Response) => {
		let idPaciente: any = req.params.idPaciente;
		var respuesta = await listarPorPaciente(idPaciente);
		return res.json(respuesta);
	};

	crearHabitoPorPaciente = async (req: Request, res: Response) => {
		let idPaciente: any = req.params.idPaciente;
		const habitos = req.body.habitos;

		var respuesta = await crearHabitoPorPaciente(habitos, idPaciente);
		return res.json(respuesta);
	};

	listarPorId = async (req: Request, res: Response) => {
		let id: any = req.params.id;

		var respuesta = await listarHabitoPorId(id);
		return res.json(respuesta);
	};

	crear = async (req: Request, res: Response) => {
		const habito = req.body as IHabito;
		delete habito._id;

		var respuesta = await crearHabito(habito);
		return res.json(respuesta);
	};

	actualizar = async (req: Request, res: Response) => {
		let id: any = req.params.id;
		const habito = req.body as IHabito;

		var respuesta = await actualizarHabito(id, habito);
		return res.json(respuesta);
	};

	eliminar = async (req: Request, res: Response) => {
		let id: any = req.params.id;

		var respuesta = await eliminarHabito(id);
		return res.json(respuesta);
	};
}
