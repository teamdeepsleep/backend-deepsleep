import { Request, Response, NextFunction } from 'express';
import { verify } from 'jsonwebtoken';
import { SECRET_TOKEN } from '../config';

export const validarToken = (
	req: Request,
	res: Response,
	next: NextFunction,
) => {
	const token: any = req.headers['x-token'];
	console.log('token capturado->', token);

	if (!token) {
		return res.status(401).json({
			operacion: false,
			mensaje: 'Token no proporcionado',
		});
	}

	try {
		const { _id, rol } = verify(token, SECRET_TOKEN) as any;
		req.params._id = _id;
		req.params.rol = rol;

		next();
	} catch (e) {
		return res.status(401).json({
			operacion: false,
			mensaje: 'Token no valido',
		});
	}
};
