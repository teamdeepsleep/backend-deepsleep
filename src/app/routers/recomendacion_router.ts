import { Router } from 'express';
import { RecomendacionController } from '../controllers/recomendacionController';

export const router = Router();
const recomendacionController: RecomendacionController =
	new RecomendacionController();

router.get('/', recomendacionController.listarTodos);

router.get('/:id', recomendacionController.listarPorId);
router.get('/paciente/:id', recomendacionController.listarPorIdPaciente);

router.post('/', recomendacionController.crear);

router.put('/:id', recomendacionController.actualizar);

router.delete('/:id', recomendacionController.eliminar);
