import { ObjectId } from 'bson';
import { IRespuesta } from '../helpers/IRespuesta';
import { IRecomendacion } from '../models/interfaces/IRecomendacion';
import Paciente from '../models/schemas/pacienteModel';
import HabitoPaciente from '../models/schemas/habitoPacienteModel';
import Recomendacion from '../models/schemas/recomendacionModel';

export const listarRecomendaciones = async (): Promise<IRecomendacion[]> => {
	try {
		return await Recomendacion.find();
	} catch (error) {
		return [];
	}
};

export const buscarRecomedacionPorId = async (id: ObjectId) => {
	try {
		return await Recomendacion.findById(id);
	} catch (error) {
		return {};
	}
};

export const buscarRecomedacionPorTipo = async (tipo: string) => {};
export const buscarRecomedacionPorIdPaciente = async (idPaciente: ObjectId) => {
	var habitoPacienteEncontrado = await HabitoPaciente.find({ idPaciente });
	console.log('id pac', idPaciente, habitoPacienteEncontrado);
	var filterHabitosList = habitoPacienteEncontrado.map(
		(habitoPorPaciente) => {
			return habitoPorPaciente.idHabito;
		},
	);
	console.log('filterHabitosList', filterHabitosList);
	var data = await Recomendacion.find({
		habitos: {
			$in: filterHabitosList,
		},
	});
	return {
		operacion: true,
		data: data,
		mensaje: 'lista encontrada',
	} as IRespuesta<IRecomendacion[]>;
};
export const crearRecomendacion = async (recomendacion: any) => {
	try {
		console.log('la recomendacion mandado es', recomendacion);
		const recomendacionNEW = new Recomendacion(recomendacion);
		//Guardar
		var data = await recomendacionNEW.save();
		return data;
	} catch (error) {
		console.log(error);
		return null;
	}
};
export const actualizarRecomendacion = async (
	id: ObjectId,
	recomendacion: any,
) => {
	try {
		const recomendacionUPDATE = new Recomendacion(recomendacion);
		var data = await Recomendacion.findByIdAndUpdate(
			{ _id: id },
			recomendacion,
			{
				new: true,
			},
		);
		return data;
	} catch (error) {
		console.log(error);
		return null;
	}
};
export const eliminarRecomendacion = async (id: ObjectId) => {
	try {
		var data = await Recomendacion.findByIdAndDelete(id);
		console.log('eliminee', data);

		return true;
	} catch (error) {
		return false;
	}
};
