export interface IRespuesta<T> {
	operacion: boolean;
	data: T | null;
	mensaje: string;
}
