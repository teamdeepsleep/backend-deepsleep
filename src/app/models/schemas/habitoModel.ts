import { Schema, model } from 'mongoose';

var habitoSchema = new Schema(
	{
		nombre: {
			type: String,
			required: true,
		},
		imagen: {
			type: String,
			required: false,
		},
	},
	{
		collection: 'habitos',
	},
);

var Habito = model('habitos', habitoSchema);
export default Habito;
