const path = require('path');
const fs = require('fs');
var FileReader = require('filereader');
import { cloudinary } from '../config/cloudinary';

import { Request, Response } from 'express';
import { v4 as uuidv4 } from 'uuid';
import { actualizarImagen } from '../helpers/actualizar-imagen';

export class UploadController {
	constructor() {}

	subirPaciente = async (req: Request, res: Response) => {
		var respuesta = {
			operacion: true,
			mensaje: 'esta bien',
			nombreArchivo: '',
		};

		try {
			const id = req.params.id;

			if (!req.files || Object.keys(req.files).length === 0) {
				return res.status(400).json({
					operacion: false,
					mensaje: 'no hay archivos',
				});
			}

			const file: any = req.files.imagen;
			// var fff = file.data;
			// var imgStr = fff.toString('base64');
			// console.log(imgStr);
			// var xd = await cloudinary.uploader.upload(
			// 	'data:image/jpeg;base64,' + imgStr,
			// 	{
			// 		upload_preset: 'deep',
			// 	},
			// );
			const nombreCortado = file.name.split('.');
			const extensionArchivo = nombreCortado[nombreCortado.length - 1];
			console.log(extensionArchivo);

			//validar extension
			const extensionValida = ['png', 'jpg', 'jpeg', 'gif'];
			if (!extensionValida.includes(extensionArchivo)) {
				return res.status(400).json({
					operacion: false,
					mensaje: 'la extensión no es permitida',
				});
			}

			var fff = file.data;
			var imgStr = fff.toString('base64');
			var dataCloudinary = await cloudinary.uploader.upload(
				'data:image/' + extensionArchivo + ';base64,' + imgStr,
				{
					upload_preset: 'deep_paciente',
				},
			);
			console.log(dataCloudinary);
			actualizarImagen('paciente', id, dataCloudinary.url);
			// //generar nombre
			// const nombreArchivo = `${uuidv4()}.${extensionArchivo}`;

			// //path para guardar la imagen
			// const path = `src/app/uploads/pacientes/${nombreArchivo}`;

			// //guardar la imagen
			// await file.mv(path, async (err: any) => {
			// 	if (err) {
			// 		console.log(err);
			// 		return res.status(400).json({
			// 			operacion: false,
			// 			mensaje: 'hubo un error al guardar la imagen',
			// 		});
			// 	}
			// 	actualizarImagen('paciente', id, nombreArchivo);
			// });

			respuesta = {
				operacion: true,
				mensaje: 'Imagen actualizada',
				nombreArchivo: dataCloudinary.url,
			};
		} catch (error) {
			console.log(error);
			respuesta = {
				operacion: false,
				mensaje: 'error',
				nombreArchivo: '',
			};
		}
		return res.json(respuesta);
	};
	subirEspecialista = async (req: Request, res: Response) => {
		var respuesta = {
			operacion: true,
			mensaje: 'esta bien',
			nombreArchivo: '',
		};

		try {
			const id = req.params.id;

			if (!req.files || Object.keys(req.files).length === 0) {
				return res.status(400).json({
					operacion: false,
					mensaje: 'no hay archivos',
				});
			}

			const file: any = req.files.imagen;
			const nombreCortado = file.name.split('.');
			const extensionArchivo = nombreCortado[nombreCortado.length - 1];

			//validar extension
			const extensionValida = ['png', 'jpg', 'jpeg', 'gif'];
			if (!extensionValida.includes(extensionArchivo)) {
				return res.status(400).json({
					operacion: false,
					mensaje: 'la extensión no es permitida',
				});
			}

			//gaurdar en Cloudinary
			var fff = file.data;
			var imgStr = fff.toString('base64');
			const dataCloudinary = await cloudinary.uploader.upload(
				'data:image/' + extensionArchivo + ';base64,' + imgStr,
				{
					upload_preset: 'deep_paciente',
				},
			);
			actualizarImagen('especialista', id, dataCloudinary.url);

			respuesta = {
				operacion: true,
				mensaje: 'Imagen actualizada',
				nombreArchivo: dataCloudinary.url,
			};
		} catch (error) {
			console.log(error);
			respuesta = {
				operacion: false,
				mensaje: 'error',
				nombreArchivo: '',
			};
		}
		return res.json(respuesta);
	};
	subirHabito = async (req: Request, res: Response) => {
		var respuesta = {
			operacion: true,
			mensaje: 'esta bien',
			nombreArchivo: '',
		};

		try {
			const id = req.params.id;

			if (!req.files || Object.keys(req.files).length === 0) {
				return res.status(400).json({
					operacion: false,
					mensaje: 'no hay archivos',
				});
			}

			const file: any = req.files.imagen;
			const nombreCortado = file.name.split('.');
			const extensionArchivo = nombreCortado[nombreCortado.length - 1];

			//validar extension
			const extensionValida = ['png', 'jpg', 'jpeg', 'gif'];
			if (!extensionValida.includes(extensionArchivo)) {
				return res.status(400).json({
					operacion: false,
					mensaje: 'la extensión no es permitida',
				});
			}

			//gaurdar en Cloudinary
			var fff = file.data;
			var imgStr = fff.toString('base64');
			const dataCloudinary = await cloudinary.uploader.upload(
				'data:image/' + extensionArchivo + ';base64,' + imgStr,
				{
					upload_preset: 'deep_paciente',
				},
			);
			actualizarImagen('habito', id, dataCloudinary.url);

			respuesta = {
				operacion: true,
				mensaje: 'Imagen actualizada',
				nombreArchivo: dataCloudinary.url,
			};
		} catch (error) {
			console.log(error);
			respuesta = {
				operacion: false,
				mensaje: 'error',
				nombreArchivo: '',
			};
		}
		return res.json(respuesta);
	};
	subirRecomendacion = async (req: Request, res: Response) => {
		var respuesta = {
			operacion: true,
			mensaje: 'esta bien',
			nombreArchivo: '',
		};

		try {
			const id = req.params.id;

			if (!req.files || Object.keys(req.files).length === 0) {
				return res.status(400).json({
					operacion: false,
					mensaje: 'no hay archivos',
				});
			}

			const file: any = req.files.imagen;
			const nombreCortado = file.name.split('.');
			const extensionArchivo = nombreCortado[nombreCortado.length - 1];

			//validar extension
			const extensionValida = ['png', 'jpg', 'jpeg', 'gif'];
			if (!extensionValida.includes(extensionArchivo)) {
				return res.status(400).json({
					operacion: false,
					mensaje: 'la extensión no es permitida',
				});
			}

			//gaurdar en Cloudinary
			var fff = file.data;
			var imgStr = fff.toString('base64');
			const dataCloudinary = await cloudinary.uploader.upload(
				'data:image/' + extensionArchivo + ';base64,' + imgStr,
				{
					upload_preset: 'deep_paciente',
				},
			);
			actualizarImagen('recomendacion', id, dataCloudinary.url);

			respuesta = {
				operacion: true,
				mensaje: 'Imagen actualizada',
				nombreArchivo: dataCloudinary.url,
			};
		} catch (error) {
			console.log(error);
			respuesta = {
				operacion: false,
				mensaje: 'error',
				nombreArchivo: '',
			};
		}
		return res.json(respuesta);
	};
	verImagen = async (req: Request, res: Response) => {
		const tipo = req.params.tipo;
		const foto = req.params.foto;

		const pathImg = path.join(__dirname, `../uploads/${tipo}/${foto}`);

		// imagen por defecto
		if (fs.existsSync(pathImg)) {
			res.sendFile(pathImg);
		} else {
			const pathImg = path.join(__dirname, `../uploads/no-img.jpg`);
			res.sendFile(pathImg);
		}
	};
}
