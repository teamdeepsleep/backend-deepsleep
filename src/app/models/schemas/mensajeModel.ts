import { Schema, model } from 'mongoose';
import { IMensaje } from '../interfaces/IMensaje';

var mensajeSchema = new Schema(
	{
		mensaje: {
			type: String,
			required: true,
		},
		idPaciente: {
			type: String,
			required: true,
		},
		idEspecialista: {
			type: String,
			required: true,
		},
	},
	{
		collection: 'mensajes',
	},
);

var Mensaje = model<IMensaje>('mensajes', mensajeSchema);
export default Mensaje;
