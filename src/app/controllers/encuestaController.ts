import { Request, Response } from 'express';
import { registrarEncuesta } from '../repositories/encuestaRepository';

export class EncuestaController {
	constructor() {}

	registrarEncuesta = async (req: Request, res: Response) => {
		const encuesta = req.body;
		console.log('registrarEncuesta', encuesta);
		const { idPaciente, resultado } = req.body;
		var respuesta = await registrarEncuesta(
			idPaciente,
			encuesta,
			resultado,
		);
		console.log(respuesta);
		return res.json(respuesta);
	};
}
