import { ObjectId } from 'bson';
import { genSalt, hash, compare, genSaltSync, hashSync } from 'bcryptjs';
import { IEspecialista } from '../models/interfaces/IEspecialista';
import Especialista from '../models/schemas/especialistaModel';
import Solicitud from '../models/schemas/solicitudModel';

export const listarEspecialistas = async (): Promise<IEspecialista[]> => {
	try {
		console.log();
		return await Especialista.find();
	} catch (error) {
		return [];
	}
};

export const listarActivos = async (): Promise<IEspecialista[]> => {
	try {
		console.log();
		return await Especialista.find({ habilitado: true });
	} catch (error) {
		return [];
	}
};

export const detalleEspecialista = async (idEspecialista: ObjectId) => {
	try {
		console.log('idE:', idEspecialista);
		var espeEncontrado = await Especialista.findById(idEspecialista);
		return {
			operacion: true,
			data: espeEncontrado,
			mensaje: 'Especialista no Enocntrado',
		};
	} catch (error) {
		return {
			operacion: false,
			data: [],
			mensaje: 'Especialista no Enocntrado',
		};
	}
};
export const misPacientes = async (idespecialista: String) => {
	try {
		console.log(idespecialista);
		var data = await Solicitud.find({
			idEspecialista: idespecialista,
			estado: 'ACEPTADO',
		}).populate({
			path: 'idPaciente',
			select: 'nombre apellido dni correo telefono imagen',
		});
		console.log('pipip', data);
		return {
			operacion: true,
			data: data,
			mensaje: 'lista encontrada',
		};
	} catch (error) {
		return {
			operacion: false,
			data: [],
			mensaje: 'lista no encontrada',
		};
	}
};

export const misPosiblesPacientes = async (idespecialista: ObjectId) => {
	try {
		console.log('idespec', idespecialista);
		var data: any = await Solicitud.find({
			idEspecialista: idespecialista,
			estado: 'PENDIENTE',
		}).populate({
			path: 'idPaciente',
			select: 'nombre apellido dni correo telefono imagen',
		});
		// delete data.password;
		return {
			operacion: true,
			data: data,
			mensaje: 'lista encontrada',
		};
	} catch (error) {
		console.log('err', error);
		return {
			operacion: false,
			data: [],
			mensaje: 'lista no encontrada',
		};
	}
};

export const miPosiblePacienteAceptar = async (
	idPaciente: String,
	idEspecialista: String,
) => {
	try {
		var dataEncontrada: any = await Solicitud.findOneAndUpdate(
			{
				idEspecialista: idEspecialista,
				idPaciente: idPaciente,
			},
			{
				estado: 'ACEPTADO',
			},
			{ new: true },
		);
		console.log(dataEncontrada);
		return {
			operacion: true,
			mensaje: 'lista encontrada',
		};
	} catch (error) {
		return {
			operacion: false,
			mensaje: 'lista no encontrada',
		};
	}
};

export const agregarEspecialidad = async (
	idEspecialista: ObjectId,
	textEspecialidad: String,
) => {
	try {
		// var f = await Especialista.findByIdAndUpdate(
		// 	{ _id: idEspecialista },
		// 	{ especialidades:  },
		// 	{ new: true },
		// );
		var especialistaEncontrado = await Especialista.findById({
			_id: idEspecialista,
		});

		await especialistaEncontrado?.especialidades.push(textEspecialidad);
		await especialistaEncontrado?.save();
		return {
			operacion: true,
			mensaje: 'actualizado',
		};
	} catch (error) {
		return {
			operacion: false,
			mensaje: 'no actualizado',
		};
	}
};

export const eliminarEspecialidad = async (
	idEspecialista: ObjectId,
	index: number,
) => {
	try {
		var especialistaEncontrado = await Especialista.findById({
			_id: idEspecialista,
		});

		await especialistaEncontrado?.especialidades.splice(index, 1);
		await especialistaEncontrado?.save();
		return {
			operacion: true,
			mensaje: 'actualizado',
		};
	} catch (error) {
		return {
			operacion: false,
			mensaje: 'no actualizado',
		};
	}
};
export const actualizarEspecialistaDetalle = async (
	idEspecialista: ObjectId,
	campo: string,
	valor: string,
) => {
	try {
		var especialistaEncontrado = await Especialista.findById({
			_id: idEspecialista,
		});
		console.log('datos', especialistaEncontrado);
		if (especialistaEncontrado) {
			if (campo == 'telefono') {
				especialistaEncontrado.telefono = valor;
			}
			if (campo == 'trabajo') {
				especialistaEncontrado.trabajo = valor;
			}

			await especialistaEncontrado?.save();
		}

		return {
			operacion: true,
			mensaje: 'actualizado',
		};
	} catch (error) {
		return {
			operacion: false,
			mensaje: 'no actualizado',
		};
	}
};

export const buscarEspecialistaPorId = async (id: ObjectId) => {
	try {
		return await Especialista.findById(id);
	} catch (error) {
		return null;
	}
};

export const crearEspecialista = async (especialista: IEspecialista) => {
	try {
		console.log('el especialista mandado es', especialista);
		const especialistaNEW = new Especialista(especialista);
		//Encriptar Contraseña
		const salt = genSaltSync();
		especialistaNEW.password = hashSync(especialista.password, salt);

		//Guardar
		var data = await especialistaNEW.save();
		return data;
	} catch (error) {
		console.log(error);
		return null;
	}
};

export const actualizarEspecialista = async (
	id: ObjectId,
	especialista: IEspecialista,
) => {
	try {
		const especialistaDB = await Especialista.findByIdAndUpdate(
			{ _id: id },
			especialista,
			{ new: true },
		);
		return especialistaDB;
	} catch (error) {
		console.log(error);
		return null;
	}
};
export const cambiarEstadoEspecialista = async (
	id: ObjectId,
	estado: boolean,
) => {
	try {
		const espeialistaDB = await Especialista.findByIdAndUpdate(
			{ _id: id },
			{ habilitado: estado },
			{ new: true },
		);
		return espeialistaDB;
	} catch (error) {
		return null;
	}
};
