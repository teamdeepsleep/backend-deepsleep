import { Router } from 'express';
import { HabitoController } from '../controllers/habitoController';

const habitoController: HabitoController = new HabitoController();
export const router = Router();

router.get('/:id', habitoController.listarPorId);
router.get('/', habitoController.listarTodos);
router.post('/', habitoController.crear);
router.put('/:id', habitoController.actualizar);
router.delete('/:id', habitoController.eliminar);
router.get('/paciente/:idPaciente', habitoController.listarPorPaciente);
router.post('/paciente/:idPaciente', habitoController.crearHabitoPorPaciente);
