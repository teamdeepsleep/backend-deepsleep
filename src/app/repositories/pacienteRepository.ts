import { IPaciente } from '../models/interfaces/IPaciente';
import Paciente from '../models/schemas/pacienteModel';
import Especialista from '../models/schemas/especialistaModel';
import Solicitud from '../models/schemas/solicitudModel';
import { ObjectId } from 'bson';
import { genSalt, hash, compare, genSaltSync, hashSync } from 'bcryptjs';
import { ISolicitud } from '../models/interfaces/ISolicitud';

export const listarPacientes = async (): Promise<IPaciente[]> => {
	try {
		console.log();
		return await Paciente.find();
	} catch (error) {
		return [];
	}
};

export const buscarPacientePorId = async (id: ObjectId) => {
	try {
		return await Paciente.findById(id);
	} catch (error) {
		return null;
	}
};
export const crearPaciente = async (paciente: IPaciente) => {
	try {
		console.log('el usuario mandado es', paciente);
		//validar si el DNI ya existe

		const pacienteNEW = new Paciente(paciente);
		//Encriptar Contraseña
		const salt = genSaltSync();
		pacienteNEW.password = hashSync(paciente.password, salt);

		//Guardar
		var data = await pacienteNEW.save();
		return data;
	} catch (error) {
		console.log(error);
		return null;
	}
};
export const actualizarPaciente = async (id: ObjectId, paciente: IPaciente) => {
	console.log(id, paciente);
	try {
		const pacienteDB = await Paciente.findByIdAndUpdate(
			{ _id: id },
			paciente,
			{ new: true },
		);
		return pacienteDB;
	} catch (error) {
		console.log(error);
		return null;
	}
};
export const cambiarEstadoPaciente = async (id: ObjectId, estado: boolean) => {
	try {
		const espeialistaDB = await Paciente.findByIdAndUpdate(
			{ _id: id },
			{ habilitado: estado },
			{ new: true },
		);
		return espeialistaDB;
	} catch (error) {
		return null;
	}
};

export const asignarEspecialista = async (
	idPaciente: ObjectId,
	idEspecialista: ObjectId,
) => {
	try {
		console.log(idPaciente, idEspecialista);
		var solicitudEncontrada = await Solicitud.findOne({
			idPaciente: idPaciente,
		});
		console.log(solicitudEncontrada);
		if (solicitudEncontrada == null) {
			console.log('solicitudEncontrada', solicitudEncontrada);
			var solicitud = {
				idPaciente: idPaciente,
				idEspecialista: idEspecialista,
			};
			const solicitudNEW = new Solicitud(solicitud);
			//Guardar
			var data = await solicitudNEW.save();
			return data;
		}

		var ds = await Solicitud.findOneAndUpdate(
			{
				idPaciente: idPaciente,
			},
			{
				idSolicitud: idEspecialista,
				estado: 'PENDIENTE',
			},
			{ new: true },
		);
		return ds;
	} catch (error) {
		return null;
	}
};

export const miespecialista = async (idPaciente: ObjectId) => {
	var devolver = {
		operacion: false,
		mensaje: 'Sin Asignacion',
		medico_imagen:
			'https://www.tenforums.com/geek/gars/images/2/types/thumb_15951118880user.png',
		medico_nombre: '',
		medico_correo: '',
		medico_telefono: '',
		// data: {
		// 	estado: '',
		// 	idMedico: '',
		// },
	};

	try {
		var solicitudEncontrada = await Solicitud.findOne({
			idPaciente: idPaciente,
		});
		if (solicitudEncontrada == null) {
			devolver.operacion = false;
			devolver.mensaje = 'no hay asignacion';

			return devolver;
		}
		console.log(solicitudEncontrada);
		var medicoEncontrado = await Especialista.findById(
			solicitudEncontrada.idEspecialista,
		);
		console.log('medicoEncontrado', medicoEncontrado);
		if (medicoEncontrado) {
			devolver.medico_nombre =
				'' + medicoEncontrado.nombre + ' ' + medicoEncontrado.apellido;
			devolver.medico_telefono = '' + medicoEncontrado.telefono;
			devolver.medico_correo = '' + medicoEncontrado.correo;

			if (medicoEncontrado.imagen) {
				devolver.medico_imagen = medicoEncontrado.imagen;
			}
		}
		devolver.operacion = true;
		devolver.mensaje = solicitudEncontrada.estado;

		console.log(solicitudEncontrada);
	} catch (error) {
		console.log(error);
		return devolver;
	}

	return devolver;
};