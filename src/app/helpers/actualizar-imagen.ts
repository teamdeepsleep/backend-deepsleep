import Paciente from '../models/schemas/pacienteModel';
import Especialista from '../models/schemas/especialistaModel';
import Recomendacion from '../models/schemas/recomendacionModel';
import { existsSync, unlinkSync } from 'fs';
import Habito from '../models/schemas/habitoModel';

const borrarImagen = (pathViejo: string) => {
	if (existsSync(pathViejo)) {
		unlinkSync(pathViejo);
	}
};

export const actualizarImagen = async (
	tipo: string,
	id: string,
	nombreArchivo: string,
) => {
	switch (tipo) {
		case 'paciente':
			const paciente = await Paciente.findById(id);
			if (!paciente) {
				return false;
			}
			console.log('actualizando imagen', nombreArchivo);
			const pathViejoPaciente2 = `src/app/uploads/pacientes/${paciente.imagen}`;
			borrarImagen(pathViejoPaciente2);

			paciente.imagen = nombreArchivo;
			await paciente.save();
			return true;

			break;
		case 'especialista':
			const especialista = await Especialista.findById(id);
			if (!especialista) {
				return false;
			}
			console.log('actualizando imagen', nombreArchivo);
			const pathViejoEspecialista2 = `src/app/uploads/especialistas/${especialista.imagen}`;
			borrarImagen(pathViejoEspecialista2);

			especialista.imagen = nombreArchivo;
			await especialista.save();
			return true;

			break;
		case 'habito':
			const habito = await Habito.findById(id);
			if (!habito) {
				return false;
			}
			console.log('actualizando imagen', nombreArchivo);
			const pathViejoHabito2 = `src/app/uploads/especialistas/${habito.imagen}`;
			borrarImagen(pathViejoHabito2);

			habito.imagen = nombreArchivo;
			await habito.save();
			return true;

			break;
		case 'recomendacion':
			const recomendacion = await Recomendacion.findById(id);
			if (!recomendacion) {
				return false;
			}
			console.log('actualizando imagen', nombreArchivo);
			const pathViejo2 = `src/app/uploads/recomendaciones/${recomendacion.imagen}`;
			borrarImagen(pathViejo2);

			recomendacion.imagen = nombreArchivo;
			await recomendacion.save();
			return true;

			break;
	}
};
