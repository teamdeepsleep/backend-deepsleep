import { Document } from 'mongoose';

export interface IPaciente extends Document {
	nombre: string;
	apellido: string;
	dni: string;
	correo: string;
	password: string;
	imagen: string;
	telefono: string;
	idEspecialista: string;
	habilitado: boolean;
	psqi: number;
}
