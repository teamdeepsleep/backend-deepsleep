import { Schema, model } from 'mongoose';
import { ISolicitud } from '../interfaces/ISolicitud';

var solicitudSchema = new Schema(
	{
		idPaciente: {
			type: Schema.Types.ObjectId,
			required: true,
			ref: 'pacientes',
		},
		idEspecialista: {
			type: String,
			required: true,
		},
		estado: {
			type: String,
			required: false,
			default: 'PENDIENTE',
		},
		fecha: {
			type: Date,
			required: false,
			default: Date.now(),
		},
	},
	{
		collection: 'solicitudes',
	},
);

var Solicitud = model<ISolicitud>('solicitudes', solicitudSchema);
export default Solicitud;
