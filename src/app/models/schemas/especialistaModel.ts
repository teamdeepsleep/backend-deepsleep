import { Schema, model } from 'mongoose';
import { IEspecialista } from '../interfaces/IEspecialista';

var especialistaSchema = new Schema(
	{
		nombre: {
			type: String,
			required: true,
		},
		apellido: {
			type: String,
			required: true,
		},
		dni: {
			type: String,
			required: true,
			unique: true,
		},
		telefono: {
			type: String,
			required: false,
		},
		correo: {
			type: String,
			required: true,
			unique: true,
		},
		especialidades: [
			{
				type: String,
				required: false,
			},
		],
		trabajo: {
			type: String,
			required: false,
		},
		imagen: {
			type: String,
			required: false,
		},
		password: {
			type: String,
			required: true,
			select: false,
		},
		habilitado: {
			type: Boolean,
			default: false,
		},
	},
	{
		collection: 'especialistas',
	},
);

var Especialista = model<IEspecialista>('especialistas', especialistaSchema);
export default Especialista;
