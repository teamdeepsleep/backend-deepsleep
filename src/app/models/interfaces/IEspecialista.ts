import { Document } from 'mongoose';

export interface IEspecialista extends Document {
	nombre: string;
	apellido: string;
	dni: string;
	correo: string;
	telefono: string;
	imagen: string;
	password: string;
	habilitado: boolean;
	especialidades: String[];
	trabajo: string;
}
