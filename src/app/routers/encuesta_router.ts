import { Router } from 'express';
import { EncuestaController } from '../controllers/encuestaController';

const encuestaController: EncuestaController = new EncuestaController();
export const router = Router();

router.post('/', encuestaController.registrarEncuesta);
