import { Schema, model } from 'mongoose';

var encuestaSchema = new Schema(
	{
		pregunta1: {
			type: String,
			required: false,
		},
		pregunta2: {
			type: Number,
			required: false,
		},
		pregunta3: {
			type: String,
			required: false,
		},
		pregunta4: {
			type: Number,
			required: false,
		},
		pregunta5A: {
			type: Number,
			required: false,
		},
		pregunta5B: {
			type: Number,
			required: false,
		},
		pregunta5C: {
			type: Number,
			required: false,
		},
		pregunta5D: {
			type: Number,
			required: false,
		},
		pregunta5E: {
			type: Number,
			required: false,
		},
		pregunta5F: {
			type: Number,
			required: false,
		},
		pregunta5G: {
			type: Number,
			required: false,
		},
		pregunta5H: {
			type: Number,
			required: false,
		},
		pregunta5I: {
			type: Number,
			required: false,
		},
		pregunta6: {
			type: Number,
			required: false,
		},
		pregunta7: {
			type: Number,
			required: false,
		},
		pregunta8: {
			type: Number,
			required: false,
		},
		pregunta9: {
			type: Number,
			required: false,
		},
		idPaciente: {
			type: Schema.Types.ObjectId,
			ref: 'usuarios',
		},
		resultado: {
			type: Number,
			required: false,
		},
	},
	{
		collection: 'encuestas',
	},
);

var Habito = model('encuestas', encuestaSchema);
export default Habito;
