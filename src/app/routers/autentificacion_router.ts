import { Router } from 'express';
import { AutentificacionController } from '../controllers/autentificacionController';

export const router = Router();
const autentificacionController: AutentificacionController =
	new AutentificacionController();

router.post('/login', autentificacionController.login);
router.post('/login/paciente', autentificacionController.loginPaciente);
router.post('/login/especialista', autentificacionController.loginEspecialista);

router.get('/renew', autentificacionController.reNewToken);
