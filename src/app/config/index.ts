require('dotenv').config();
// import dotenv from 'dotenv';

// dotenv.config();
const PORT = process.env.PORT || 3001;
const MONGO_URI =
	process.env.MONGO_URI ||
	'mongodb+srv://admin:admin@cluster0.0dhs4.mongodb.net';
const SECRET_TOKEN = process.env.SECRET_TOKEN || 'MI_KEY_IS_DEEPSLEEP';

export { PORT, MONGO_URI, SECRET_TOKEN };
