// import mongoose from "mongoose";
import { connect, set } from 'mongoose';

export const startConexion = (MONGO_URI: string) => {
	// try {
	// 	const db = await connect(MONGO_URI);
	// 	console.log('db conexión exitosa', db);
	// } catch (error) {
	// 	console.error('db conexión fracasada', error);
	// }
	connect(MONGO_URI)
		.then((db) => console.log('db conexión exitosa'))
		.catch((e) => console.log('db conexión fracasada', e));
};
