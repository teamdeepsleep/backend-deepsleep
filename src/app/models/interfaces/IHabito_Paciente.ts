import { Document } from 'mongoose';

export interface IHabitoPaciente extends Document {
	idPaciente: string;
	idHabito: string;
}
