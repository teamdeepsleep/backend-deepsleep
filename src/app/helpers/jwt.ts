import { sign } from 'jsonwebtoken';
import { SECRET_TOKEN } from '../config';

export const crearToken = async (_id: string, rol: string): Promise<string> => {
	var payload = {
		_id: _id,
		rol: rol,
	};
	var opciones = {
		expiresIn: '1000h',
	};

	var token = await sign(payload, SECRET_TOKEN, opciones);
	return token;
};
