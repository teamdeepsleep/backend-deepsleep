import { Router } from 'express';
import { PacienteController } from '../controllers/pacienteController';

const pacienteController: PacienteController = new PacienteController();
export const router = Router();

router.get('/:id', pacienteController.listarPorId);
router.get('/psqi/:id', pacienteController.listarPsqiPorId);

router.put('/estado', pacienteController.cambiarEstado);

router.get('/miespecialista/:idPaciente', pacienteController.miespecialista);
router.post(
	'/miespecialista/:idespecialista',
	pacienteController.asignarEspecialista,
);

router.get('/', pacienteController.listarTodos);

router.post('/', pacienteController.crear);

router.put('/:id', pacienteController.actualizar);
