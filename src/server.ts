import express from 'express';
import morgan from 'morgan';
import cors from 'cors';

import { PORT, MONGO_URI } from './app/config';
import { startConexion } from './app/db/conexion';
import { router as routerAutentificacion } from './app/routers/autentificacion_router';
import { router as routerUpload } from './app/routers/upload_router';
import { router as routerPaciente } from './app/routers/paciente_router';
import { router as routerRecomendacion } from './app/routers/recomendacion_router';
import { router as routerEspecialista } from './app/routers/especialista_router';
import { router as routerHabito } from './app/routers/habito_router';
import { router as routerEncuesta } from './app/routers/encuesta_router';
// import fileUpload from 'express-fileupload';
const fileupload = require('express-fileupload');

export class Server {
	public app: express.Application;

	constructor() {
		this.app = express();
		// this.app.use(fileupload());
		this.config();
		this.routes();
	}

	config = async () => {
		//db
		startConexion(MONGO_URI);

		// setting
		this.app.set('port', PORT);

		// middlewares
		// this.app.use(
		// 	fileUpload({
		// 		useTempFiles: true,
		// 		limits: { files: 4 },
		// 	}),
		// );
		// this.app.use((req: any, res: any, next: any) => {
		// 	console.log('user :  ' + req.user + ' ===== files : ' + req.files);
		// 	next();
		// });
		this.app.use(cors());
		this.app.use(morgan('dev'));
		this.app.use(express.json());
		this.app.use(express.urlencoded({ extended: false }));
	};

	routes = () => {
		this.app.use('/api/auth', routerAutentificacion);
		this.app.use('/api/upload', routerUpload);
		this.app.use('/api/paciente', routerPaciente);
		this.app.use('/api/recomendacion', routerRecomendacion);
		this.app.use('/api/especialista', routerEspecialista);
		this.app.use('/api/habito', routerHabito);
		this.app.use('/api/encuesta', routerEncuesta);
		this.app.use('/', (req, res) => res.send('Hola 👻🤓🧐 la APi: /api/'));
	};

	start = () => {
		this.app.listen(this.app.get('port'), () =>
			console.log('Server en ', this.app.get('port')),
		);
	};
}
