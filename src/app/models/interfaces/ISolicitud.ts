import { Document } from 'mongoose';

export interface ISolicitud extends Document {
	idPaciente: string;
	idEspecialista: string;
	estado: string;
	fecha: Date;
}
