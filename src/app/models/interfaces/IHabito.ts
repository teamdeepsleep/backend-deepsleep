import { Document } from 'mongoose';

export interface IHabito extends Document {
	imagen: string;
	nombre: string;
}
