import { Request, Response } from 'express';
import { IPaciente } from '../models/interfaces/IPaciente';
import {
	listarPacientes,
	buscarPacientePorId,
	crearPaciente,
	cambiarEstadoPaciente,
	actualizarPaciente,
	asignarEspecialista,
	miespecialista,
} from '../repositories/pacienteRepository';

export class PacienteController {
	constructor() {}

	listarTodos = async (req: Request, res: Response) => {
		var respuesta = await listarPacientes();
		return res.json(respuesta);
	};

	listarPorId = async (req: Request, res: Response) => {
		let id: any = req.params.id;

		var respuesta = await buscarPacientePorId(id);
		if (respuesta === null) {
			return res.json({ usuario: 'no existe' });
		}
		return res.json(respuesta);
	};

	listarPsqiPorId = async (req: Request, res: Response) => {
		let id: any = req.params.id;

		var respuesta = await buscarPacientePorId(id);
		if (respuesta === null) {
			return res.json({ usuario: 'no existe' });
		}
		return res.json(respuesta);
	};

	crear = async (req: Request, res: Response) => {
		const paciente = req.body as IPaciente;
		var respuesta = await crearPaciente(paciente);
		return res.json({
			operacion: true,
			data: respuesta,
		});
	};

	actualizar = async (req: Request, res: Response) => {
		const paciente = req.body as IPaciente;
		const id: any = req.params.id;
		console.log(id, paciente);
		var respuesta = await actualizarPaciente(id, paciente);
		return res.json({
			operacion: true,
			data: respuesta,
		});
	};

	cambiarEstado = async (req: Request, res: Response) => {
		const { id, estado } = req.body;

		var respuesta = await cambiarEstadoPaciente(id, estado);
		return res.json({
			operacion: true,
			data: respuesta,
		});
	};

	miespecialista = async (req: Request, res: Response) => {
		const idPaciente: any = req.params.idPaciente;

		var respuesta = await miespecialista(idPaciente);
		// return res.json({
		// 	operacion: true,
		// 	data: respuesta,
		// });

		return res.json(respuesta);
	};

	asignarEspecialista = async (req: Request, res: Response) => {
		const idespecialista: any = req.params.idespecialista;
		const { id } = req.body;

		var respuesta = await asignarEspecialista(id, idespecialista);
		return res.json({
			operacion: true,
			data: respuesta,
		});

		// return res.json({});
	};
}
