import { Document, Schema } from 'mongoose';

export interface IRecomendacion extends Document {
	imagen: string;
	titulo: string;
	descripcion: string;
	habitos: Schema.Types.ObjectId[][];
}
