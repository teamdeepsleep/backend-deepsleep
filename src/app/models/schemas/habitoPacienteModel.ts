import { Schema, model } from 'mongoose';

var habitoPacienteSchema = new Schema(
	{
		idPaciente: {
			type: Schema.Types.ObjectId,
			ref: 'usuarios',
		},
		idHabito: {
			type: Schema.Types.ObjectId,
			ref: 'habitos',
		},
	},
	{
		collection: 'habito_paciente',
	},
);

var HabitoPaciente = model('habito_paciente', habitoPacienteSchema);
export default HabitoPaciente;
