import { Request, Response } from 'express';
import { IEspecialista } from '../models/interfaces/IEspecialista';
import {
	crearEspecialista,
	listarEspecialistas,
	cambiarEstadoEspecialista,
	listarActivos,
	misPacientes,
	misPosiblesPacientes,
	buscarEspecialistaPorId,
	miPosiblePacienteAceptar,
	agregarEspecialidad,
	detalleEspecialista,
	eliminarEspecialidad,
	actualizarEspecialistaDetalle,
} from '../repositories/especialistaRepository';
export class EspecialistaController {
	constructor() {}

	listarTodos = async (req: Request, res: Response) => {
		var respuesta = await listarEspecialistas();
		return res.json(respuesta);
	};

	listarActivos = async (req: Request, res: Response) => {
		var respuesta = await listarActivos();
		return res.json(respuesta);
	};

	detalleEspecialista = async (req: Request, res: Response) => {
		const idespecialista: any = req.params.idespecialista;

		var respuesta = await detalleEspecialista(idespecialista);
		return res.json(respuesta);
	};

	mispacientes = async (req: Request, res: Response) => {
		const idespecialista: any = req.params.idespecialista;

		var respuesta = await misPacientes(idespecialista);
		return res.json(respuesta);
	};

	misposiblespacientes = async (req: Request, res: Response) => {
		const idespecialista: any = req.params.idespecialista;

		var respuesta = await misPosiblesPacientes(idespecialista);
		return res.json(respuesta);
	};

	miPosiblePacienteAceptar = async (req: Request, res: Response) => {
		const idEspecialista: String = req.params.idespecialista as String;
		const idPaciente: String = req.params.idpaciente as String;

		var respuesta = await miPosiblePacienteAceptar(
			idPaciente,
			idEspecialista,
		);
		console.log('se acepto ?', respuesta);
		return res.json(respuesta);
	};

	agregarEspecialidad = async (req: Request, res: Response) => {
		const idEspecialista: any = req.params.idespecialista as String;
		const { textEspecialidad } = req.body;

		var respuesta = await agregarEspecialidad(
			idEspecialista,
			textEspecialidad,
		);
		console.log(
			'se agrego especialidad ' + textEspecialidad + '?',
			respuesta,
		);
		return res.json(respuesta);
	};

	eliminarEspecialidad = async (req: Request, res: Response) => {
		const idEspecialista: any = req.params.idespecialista as String;
		const { index } = req.body;

		var respuesta = await eliminarEspecialidad(idEspecialista, index);
		console.log('se elimino especialidad ' + index + '?', respuesta);
		return res.json(respuesta);
	};

	actualizarEspecialistaDetalle = async (req: Request, res: Response) => {
		const idEspecialista: any = req.params.idespecialista as String;
		const { campo, valor } = req.body;
		console.log('raaaaaa', campo, valor);
		var respuesta = await actualizarEspecialistaDetalle(
			idEspecialista,
			campo,
			valor,
		);
		console.log('se actualizo especialista ' + valor + '?', respuesta);
		return res.json(respuesta);
	};

	listarPorId = async (req: Request, res: Response) => {
		let id: any = req.params.id;

		var respuesta = await buscarEspecialistaPorId(id);
		if (respuesta === null) {
			return res.json({
				operacion: false,
				data: respuesta,
				mensaje: 'especualista bo encontrado',
			});
		}
		return res.json({
			operacion: true,
			data: respuesta,
			mensaje: 'especualista encontrado',
		});
	};

	crear = async (req: Request, res: Response) => {
		const especialista = req.body as IEspecialista;
		var respuesta = await crearEspecialista(especialista);
		return res.json({
			operacion: true,
			data: respuesta,
		});
	};

	actualizar = async (req: Request, res: Response) => {};

	cambiarEstado = async (req: Request, res: Response) => {
		const { id, estado } = req.body;

		var respuesta = await cambiarEstadoEspecialista(id, estado);
		return res.json({
			operacion: true,
			data: respuesta,
		});
	};
}
