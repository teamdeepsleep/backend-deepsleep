import { Router } from 'express';
import expressFileUpload = require('express-fileupload');
import { UploadController } from '../controllers/uploadController';

const uploadController: UploadController = new UploadController();
export const router = Router();

router.use(expressFileUpload());

router.put('/paciente/:id', uploadController.subirPaciente);
router.put('/especialista/:id', uploadController.subirEspecialista);
router.put('/recomendacion/:id', uploadController.subirRecomendacion);
router.put('/habito/:id', uploadController.subirHabito);
router.get('/imagen/:tipo/:foto', uploadController.verImagen);