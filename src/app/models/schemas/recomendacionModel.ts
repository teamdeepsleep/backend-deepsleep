import { Schema, model } from 'mongoose';

var recomendacionSchema = new Schema(
	{
		titulo: {
			type: String,
			required: true,
		},
		descripcion: {
			type: String,
			required: true,
		},
		imagen: {
			type: String,
			required: false,
		},
		habitos: [
			{
				type: Schema.Types.ObjectId,
				ref: 'habitos',
			},
		],
	},
	{
		collection: 'recomendaciones',
	},
);

var Recomendacion = model('recomendaciones', recomendacionSchema);
export default Recomendacion;
