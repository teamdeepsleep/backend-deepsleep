import { Request, Response } from 'express';
import { IRecomendacion } from '../models/interfaces/IRecomendacion';
import {
	crearRecomendacion,
	listarRecomendaciones,
	buscarRecomedacionPorId,
	actualizarRecomendacion,
	eliminarRecomendacion,
	buscarRecomedacionPorIdPaciente,
} from '../repositories/recomendacionRepository';
export class RecomendacionController {
	constructor() {}

	listarTodos = async (req: Request, res: Response) => {
		var respuesta = await listarRecomendaciones();
		return res.json(respuesta);
	};

	listarPorId = async (req: Request, res: Response) => {
		let id: any = req.params.id;
		var respuesta = await buscarRecomedacionPorId(id);
		return res.json(respuesta);
	};
	listarPorIdPaciente = async (req: Request, res: Response) => {
		let idPaciente: any = req.params.id;
		var respuesta = await buscarRecomedacionPorIdPaciente(idPaciente);
		return res.json(respuesta);
	};
	crear = async (req: Request, res: Response) => {
		const recomendacion = req.body as IRecomendacion;
		delete recomendacion._id;
		var respuesta = await crearRecomendacion(recomendacion);
		return res.json({
			operacion: true,
			data: respuesta,
		});
	};

	actualizar = async (req: Request, res: Response) => {
		let id: any = req.params.id;
		const recomendacion = req.body as IRecomendacion;
		var respuesta = await actualizarRecomendacion(id, recomendacion);
		return res.json({
			operacion: true,
			data: respuesta,
		});
	};

	eliminar = async (req: Request, res: Response) => {
		let id: any = req.params.id;
		var respuesta = await eliminarRecomendacion(id);
		return res.json({
			operacion: true,
			data: respuesta,
		});
	};
}
